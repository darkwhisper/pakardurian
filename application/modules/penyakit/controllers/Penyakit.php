<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penyakit extends CI_Controller
{
    // form validasi input
    public $form_conf = array(
        array('field' => 'nama_penyakit', 'label' => 'Nama Penyakit', 'rules' => 'required'),
        array('field' => 'keterangan', 'label' => 'Keterangan', 'rules' => 'required'),
        array('field' => 'penanganan', 'label' => 'Penanganan', 'rules' => 'required'),
    );

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('model_penyakit');
    }

    public function index()
    {
        if (empty($this->session->userdata('id_admin'))) {
            show_404();
        }
        $this->template->title = 'Penyakit';
        $this->template->content->view('view_penyakit');
        $this->template->js->view('js_penyakit');
        // Publish the template
        $this->template->publish();
    }

    public function tampil_penyakit()
    {
        if (empty($this->session->userdata('id_admin'))) {
            show_404();
        }
        // load library cldatatable
        $this->load->library('cldatatable');
        // tombol untuk edit & hapus di dalam tabel view
        $tombol = '<div class="text-center">
                        <button type="button" class="btn btn-success ubah-data" data-id="{{id_penyakit}}"  data-toggle="tooltip"> <i class="fa fa-edit"></i> </button>
                        <button type="button" class="btn btn-danger hapus-data" data-id="{{id_penyakit}}"  data-toggle="tooltip" title="Hapus Data"><i class="fa fa-trash"></i> </button>
                    </div>';
        // set data yang akan di tampilkan di tabel
        return $this->output->set_output($this->cldatatable->set_kolom('id_penyakit, nama_penyakit, keterangan, penanganan')
            ->set_tabel('penyakit')
            ->tambah_kolom('aksi', $tombol)
            ->get_datatable());
    }

    public function user_penyakit()
    {
        // load view
        $data['m_data'] = $this->model_penyakit->get_daftarpenyakit();
        $this->template->title = 'Daftar Penyakit';
        $this->template->content->view('view_user_penyakit', $data);

        $this->template->publish();
    }

    public function tambah_penyakit()
    {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        // load and run form validasi
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->form_conf);

        if ($this->form_validation->run($this) == false) {
            $data['pesan']  = validation_errors();
            $data['status'] = false;
            return $this->output->set_output(json_encode($data));
        }

        $id = $this->model_penyakit->id_penyakit();

        $simpan['id_penyakit'] = 'P' . $id;
        $simpan['nama_penyakit'] = $this->input->post('nama_penyakit', true);
        $simpan['keterangan'] = $this->input->post('keterangan', true);
        $simpan['penanganan'] = $this->input->post('penanganan', true);

        // load to model tambah data
        if ($this->model_penyakit->tambah_penyakit($simpan)) {
            $result['status']   = true;
            $result['pesan']    = 'Data penyakit berhasil ditambahkan.';
        } else {
            $error_db           = $this->model_penyakit->get_db_error();
            $result['status']   = false;
            $result['pesan']    = 'Data penyakit gagal ditambahkan.  Kesalahan kode : ' . $error_db['code'];
        }

        return $this->output->set_output(json_encode($result));
    }

    public function get_ubahpenyakit()
    {

        if (!$this->input->is_ajax_request()) {
            return;
        }

        $id_penyakit = $this->input->post('data_id');

        $result = $this->model_penyakit->get_detail_penyakit($id_penyakit);

        if (!empty($result)) {
            $data['status'] = true;
            $data['data']   = $result;
        } else {
            $data['status'] = false;
            $data['data']   = null;
            $data['pesan']  = $this->model_penyakit->get_error_message();
        }
        $this->output->set_output(json_encode($data));
    }

    public function ubah_penyakit()
    {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->form_conf);

        if ($this->form_validation->run($this) == false) {
            $data['pesan']  = validation_errors();
            $data['status'] = false;
            return $this->output->set_output(json_encode($data));
        }

        $id = $this->input->post('id_penyakit');
        $save['nama_penyakit'] = $this->input->post('nama_penyakit');
        $save['keterangan'] = $this->input->post('keterangan');
        $save['penanganan'] = $this->input->post('penanganan');

        if ($this->model_penyakit->ubah_penyakit($save, $id, $this->input->post('id_penyakit')) == false) {
            $eror             = $this->model_penyakit->get_db_error();
            $result['status'] = false;
            $result['pesan']  = 'Data gagal diubah. Eror kode : ' . $eror['code'];
        } else {
            $result['status'] = true;
            $result['pesan']  = 'Data berhasil diubah.';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function hapus_penyakit()
    {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if ($this->input->post() == '') {
            $respon['status'] = false;
            $respon['pesan']  = 'Data ID tidak tersedia';
        }

        $hapus['id_penyakit'] = $this->input->post('data_id');

        if ($this->model_penyakit->hapus_penyakit($hapus)) {
            $data['status'] = true;
            $data['pesan']  = 'Data berhasil dihapus.';
        } else {
            $error          = $this->model_penyakit->get_db_error();
            $data['status'] = false;
            $data['pesan']  = 'Data produk gagal dihapus. Error kode : ' . $error['code'];
        }

        return $this->output->set_output(json_encode($data));
    }
}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */