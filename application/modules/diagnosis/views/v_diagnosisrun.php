<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        .jumbotron {
            position: relative;
            background: url(assets/images/background/durian-bg.jpg) center center;
            color: #fff;
            width: 100%;
            height: 100%;
            background-size: cover;
            overflow: hidden;
        }
    </style>
    <title>Beranda</title>
</head>

<body>
    <div class="row">
        <!-- membuat jumbotron -->
        <div class="jumbotron">
            <center>
                <h1 style="color: white" style="font-size: 30px"><b>Sistem Pakar Diagnosis Penyakit Tanaman Durian</b></h1><br>
                <h3 style="color: white">Aplikasi pakar yang digunakan untuk mendiagnosis penyakit tanaman durian dan solusi penanganannya</h3><br />
                <div id="#tombol-simpan"> <a class="btn btn-lg btn-warning" href="<?= site_url('diagnosis/proses') ?>">Mulai Diagnosis</a></div>
            </center>
        </div>
    </div>
</body>

</html>