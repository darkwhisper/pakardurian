<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Hasil Diagnosis Penyakit Tanaman Durian</h3>
                <h5 class="card-subtitle">Hasil Diagnosis Penyakit Tanaman Durian Berdasarkan Gejala Yang Dipilih </h5>
                <table data-toggle="table" data-height="300" data-mobile-responsive="true" class="table-striped">
                    <tr>
                        <th width="25%">Gejala Yang Dipilih</th>
                        <td>
                            <?php foreach ($pilih as $p) {
                            ?>
                                <li><?php echo $p['nama_gejala'] ?> [<?php echo $p['id_gejala'] ?>]</li>
                            <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    foreach ($penyakit as $v) {
                    ?>
                        <tr>
                            <th width="25%">Kemungkinan Penyakit Yang Terjadi</th>
                            <td><?php echo $v['nama_penyakit'] ?></td>
                        </tr>
                        <tr>
                            <th>Nilai Keakuratan Penyakit</th>
                            <td><?php echo $persentase ?>%</td>
                        </tr>
                        <tr>
                            <th>Keterangan Penyakit</th>
                            <td><?php echo $v['keterangan'] ?></td>
                        </tr>
                        <tr>
                            <th>Solusi/ Penanganan Penyakit</th>

                            <td><?php echo $v['penanganan'] ?></td>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    <?php
                    ?>
                    <?php
                    ?>
                </table>
                <br>
                <div class="btn-group ">
                    <a href="<?= site_url('diagnosis/pdf') ?>" class="btn btn-md btn-success pull-left">Cetak PDF</a>
                    <a href="<?= site_url('diagnosis/selesai') ?>" class="btn btn-md btn-danger pull-left">Selesai</a>
                </div>
            </div>
        </div>
    </div>
</div>