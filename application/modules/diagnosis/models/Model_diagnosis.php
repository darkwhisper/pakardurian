<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_diagnosis extends CI_Model
{
    function get_detail_penyakit($id_penyakit)
    {
        $query = $this->db->select('id_penyakit, nama_penyakit, keterangan, penanganan')
            ->from('penyakit')
            ->where('id_penyakit', $id_penyakit)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_gejala()
    {
        $query = $this->db->select('nama_gejala,id_gejala')
            ->from('gejala')
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function jmlgejala()
    {
        // $id_gejala ='G001';
        $query = $this->db->select('COUNT(id_gejala)')
            ->from('gejala')
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    
    function get_persentase($dataid)
    {
        $query = $this->db->select('COUNT(id_gejala) as hasil')
            ->from('aturan')
            ->where('id_penyakit', $dataid)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_penyakit($dataid)
    {
        $query = $this->db->select('nama_penyakit, keterangan, penanganan')
            ->from('penyakit')
            ->where('id_penyakit', $dataid)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_gejala_all($datamu)
    {

        $query = $this->db->select('id_penyakit')
            ->from('aturan')
            ->where_in('id_gejala', $datamu)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function gejala_pilih ($id) {
         $query = $this->db->select('nama_gejala, id_gejala')
            ->from('gejala')
            ->where_in('id_gejala', $id)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }  
}
