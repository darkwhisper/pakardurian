<script>
    $(document).ready(function() {
        dtaturan = $("#taturan").DataTable({
            "ajax": {
                "url": "<?= base_url('aturan/tampil_aturan'); ?>",
                "type": "POST"
            },
            "serverSide": true,
            "bFilter": false,
            "lengthMenu": [
                [5, 25, 50, -1],
                [5, 10, 25, "All"]
            ],
            "paging": true,
            "columns": [{
                    "data": "id_aturan"
                },
                {
                    "data": "nama_penyakit"
                },
                {
                    "data": "nama_gejala"
                },
                {
                    "data": "aksi",
                    "orderable": false
                }
            ]
        });

        // ajax untuk modal tambah data 
        $('#form-tambah').submit(function() {
            var modal = '#modal-tambah';
            var form = '#form-tambah';

            $.ajax({
                url: "<?php echo base_url('aturan/tambah_aturan') ?>",
                type: "POST",
                data: $(this).serialize(),
                timeout: 5000,
                dataType: "JSON",
                success: function(data) {
                    if (data.status) {
                        notif.success(data.pesan, "Berhasil");
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        dtaturan.ajax.reload(null, false);
                    } else {
                        notif.error(data.pesan, 'Gagal');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertajax.error(textStatus, jqXHR.status);
                }
            });
            $(form + ' #tombol-simpan').html('<i class="fa fa-save"></i> Simpan');
            $(form + ' #tombol-simpan').attr('disabled', false);
        });

            $("#tambah").select2({ width: '100%'});

        // ajax untuk hapus data
        $('#taturan').on('click', '.hapus-data', function() {
            /* var modal dan form */
            var data_id = $(this).attr('data-id');
            $.confirm({
                title: 'Hapus data?',
                content: 'Apakah Anda yakin akan menghapus data ini?',
                type: 'red',
                buttons: {
                    ya: {
                        btnClass: 'btn-red',
                        action: function() {
                            $.ajax({
                                type: 'post',
                                url: '<?php echo site_url('aturan/hapus_aturan'); ?>',
                                data: 'data_id=' + data_id,
                                dataType: 'JSON',
                                timeout: 5000,
                                success: function(data) {
                                    if (data.status) {
                                        notif.success(data.pesan, "Berhasil");
                                        dtaturan.ajax.reload(null, false);
                                    } else {
                                        notif.error(data.pesan, "Gagal");
                                        dtaturan.ajax.reload(null, false);
                                    }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    dtproduk.ajax.reload(null, false);
                                    alertajax.error(textStatus, jqXHR.status);
                                }
                            });
                        }
                    },
                    batal: function() {}
                }
            });
        });

        // ajax untuk ubah data
        $('#taturan').on('click', '.ubah-data', function() {
            /* var modal dan form */
            var modal = '#modal-ubah';
            var form = '#form-ubah';

            var data_id = $(this).attr('data-id');

            $.ajax({
                url: "<?= base_url('aturan/get_ubahaturan') ?>",
                type: "POST",
                data: "data_id=" + data_id,
                timeout: 5000,
                dataType: "JSON",
                success: function(data) {
                    if (data.status) {
                        $(form + ' [name="penyakit"]').val(data.data.id_penyakit);
                        $(form + ' [name="gejala"]').val(data.data.id_gejala);
                        $(form + ' [name="id_aturan"]').val(data.data.id_aturan);

                        $(modal).modal('show');
                    } else {
                        notif.alert('Terjadi kesalahan saat menghubungkan ke server.<br/>' +
                            data.pesan);
                    }
                 $("#ubah").select2({ width: '100%'});
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertajax.error(textStatus, jqXHR.status);
                }
            });
        });
        $('#form-ubah').submit(function() {
            /* var modal dan form */
            var modal = '#modal-ubah';
            var form = '#form-ubah';

            $.ajax({
                url: "<?php echo base_url('aturan/ubah_aturan') ?>",
                type: "POST",
                data: $(this).serialize(),
                timeout: 5000,
                dataType: "JSON",
                success: function(data) {
                    if (data.status) {
                        notif.success(data.pesan, "Berhasil");
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        dtaturan.ajax.reload(null, false);
                    } else {
                        notif.error(data.pesan, 'Gagal');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertajax.error(textStatus, jqXHR.status);
                }
            });
            $(form + ' #tombol-submit').attr('disabled', false);
        })
    })
</script>

<script type="text/javascript">
    $(function() {
        paginate();
        function paginate() {
            $('#ajax_links a').click(function() {
                var url = $(this).attr('href');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: 'ajax=true',
                    success: function(data) {
                        $('#ajax_content').html(data);
                    }
                });
                return false;
            });
        }
    });
</script>