<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gejala extends CI_Controller
{
    // form validasi input
    public $form_conf = array(
        array('field' => 'nama_gejala', 'label' => 'Nama gejala', 'rules' => 'required'),


    );

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('model_gejala');
    }

    public function index()
    {

        if (empty($this->session->userdata('id_admin'))) {
            show_404();
        }
        $this->template->title = 'Gejala';
        $this->template->content->view('view_gejala');
        $this->template->js->view('js_gejala');
        // Publish the template
        $this->template->publish();
    }

    public function tampil_gejala()
    {
        if (empty($this->session->userdata('id_admin'))) {
            show_404();
        }
        $this->load->library('cldatatable');
        // tombol untuk edit & hapus di dalam tabel view
        $tombol = '<div class="text-center">
                        <button type="button" class="btn btn-success ubah-data" data-id="{{id_gejala}}"  data-toggle="tooltip"> <i class="fa fa-edit"></i> </button>
                        <button type="button" class="btn btn-danger hapus-data" data-id="{{id_gejala}}"  data-toggle="tooltip" title="Hapus Data"><i class="fa fa-trash"></i> </button>
                    </div>';
        // set data yang akan di tampilkan di tabel
        return $this->output->set_output($this->cldatatable->set_kolom('id_gejala,nama_gejala')
            ->set_tabel('gejala')
            ->tambah_kolom('aksi', $tombol)
            ->get_datatable());
    }

    public function tambah_gejala()
    {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        // load and run form validasi
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->form_conf);

        if ($this->form_validation->run($this) == false) {
            $data['pesan']  = validation_errors();
            $data['status'] = false;
            return $this->output->set_output(json_encode($data));
        }

        $id = $this->model_gejala->id_gejala();

        $simpan['id_gejala'] = 'G' . $id;
        $simpan['nama_gejala'] = $this->input->post('nama_gejala', true);
        // print_r($simpan);
        // exit();

        // load to model tambah data
        if ($this->model_gejala->tambah_gejala($simpan)) {
            $result['status']   = true;
            $result['pesan']    = 'Data gejala berhasil ditambahkan.';
        } else {
            $error_db           = $this->model_admin->get_db_error();
            $result['status']   = false;
            $result['pesan']    = 'Data gejala gagal ditambahkan.  Kesalahan kode : ' . $error_db['code'];
        }

        return $this->output->set_output(json_encode($result));
    }

    public function get_ubahgejala()
    {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $id_gejala = $this->input->post('data_id');

        $result = $this->model_gejala->get_detail_gejala($id_gejala);

        if (!empty($result)) {
            $data['status'] = true;
            $data['data']   = $result;
        } else {
            $data['status'] = false;
            $data['data']   = null;
            $data['pesan']  = $this->model_gejala->get_error_message();
        }
        $this->output->set_output(json_encode($data));
    }

    public function ubah_gejala()
    {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->form_conf);

        if ($this->form_validation->run($this) == false) {
            $data['pesan']  = validation_errors();
            $data['status'] = false;
            return $this->output->set_output(json_encode($data));
        }

        $id = $this->input->post('id_gejala');
        $save['nama_gejala'] = $this->input->post('nama_gejala');


        if ($this->model_gejala->ubah_gejala($save, $id, $this->input->post('id_gejala')) == false) {
            $eror             = $this->model_gejala->get_db_error();
            $result['status'] = false;
            $result['pesan']  = 'Data gagal diubah. Eror kode : ' . $eror['code'];
        } else {
            $result['status'] = true;
            $result['pesan']  = 'Data berhasil diubah.';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function hapus_gejala()
    {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if ($this->input->post() == '') {
            $respon['status'] = false;
            $respon['pesan']  = 'Data ID tidak tersedia';
        }

        $hapus['id_gejala'] = $this->input->post('data_id');

        if ($this->model_gejala->hapus_gejala($hapus)) {
            $data['status'] = true;
            $data['pesan']  = 'Data berhasil dihapus.';
        } else {
            $error          = $this->model_admin->get_db_error();
            $data['status'] = false;
            $data['pesan']  = 'Data produk gagal dihapus. Error kode : ' . $error['code'];
        }

        return $this->output->set_output(json_encode($data));
    }
}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */