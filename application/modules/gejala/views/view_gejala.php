<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <button class="btn float-right hidden-sm-down btn-success" data-toggle="modal"
                    data-target="#modal-tambah"><i class="mdi mdi-plus-circle"></i>Tambah</button>
                <h4 class="card-title">Data gejala</h4>
                <div class="table-responsive m-t-40">
                    <table id="tgejala" class="display nowrap table table-hover table-striped table-bordered"
                        cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama gejala</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL TAMBAH DATA ADMIN -->
<div class="modal fade" id="modal-tambah" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="block-title">Tambah gejala</h3>
                <div class="block-options">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-tambah" onsubmit="return false">
                    <div class="block-content">
                        <input name="<?php echo $this->security->get_csrf_token_name(); ?>"
                            value="<?php echo $this->security->get_csrf_hash(); ?>" type="hidden" />
                        <div class="form-group">
                            <label class="text-bold">Nama gejala</label>
                            <input type="text" name="nama_gejala" class="form-control">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-md btn-success pull-right" id="tombol-submit">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-dark pull-right">
                            <i class="fa fa-close"></i> Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ubah" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="block-title">Ubah gejala</h3>
                <div class="block-options">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-ubah" onsubmit="return false">
                    <div class="block-content">
                        <input name="<?php echo $this->security->get_csrf_token_name(); ?>"
                            value="<?php echo $this->security->get_csrf_hash(); ?>" type="hidden" />
                        <input type="hidden" name="id_gejala" />
                        <div class="form-group">
                            <label class="text-bold">Nama gejala</label>
                            <input type="text" name="nama_gejala" class="form-control">
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-md btn-success pull-right" id="tombol-submit">
                                <i class="fa fa-save"></i> Simpan
                            </button>
                            <button type="button" data-dismiss="modal" class="btn btn-md btn-dark pull-right">
                                <i class="fa fa-close"></i> Cancel
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>