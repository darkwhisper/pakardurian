<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_admin extends CI_Model
{
    // hapus_admin
    function hapus_admin($hapus)
    {
        return $this->db->delete('admin', $hapus);
    }

    // tambah_admin
    function tambah_admin($simpan)
    {
        return $this->db->insert('admin', $simpan);
    }
    
    // ubah_admin
    function ubah_admin($save, $id)
    {
        return $this->db->update('admin', $save, array('id_admin' => $id));
    }

    function get_detail_admin($admin_id)
    {
        $query = $this->db->select('id_admin, nama, username')
            ->from('admin')
            ->where('id_admin', $admin_id)
            ->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function id_admin()
    {
        $sql = $this->db->select('CAST(SUBSTR(id_admin,3,4) AS INT) AS no')
            ->from('admin')
            ->order_by('id_admin', 'desc')
            ->limit(1)
            ->get();

        if ($sql->num_rows() > 0) {
            $result = $sql->row_array();
            $sql->free_result();
            $i     = $result['no'] + 1;
            $nomor = '00' . $i;
            return substr($nomor, -3);
        } else {
            return '001';
        }
    }
}