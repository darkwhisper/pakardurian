<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <button class="btn float-right hidden-sm-down btn-success" data-toggle="modal"
                    data-target="#modal-tambah"><i class="mdi mdi-plus-circle"></i>Tambah</button>
                <h4 class="card-title">Data Admin</h4>
                <div class="table-responsive m-t-40">
                    <table id="tadmin" class="display nowrap table table-hover table-striped table-bordered"
                        cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL TAMBAH DATA ADMIN -->
<div class="modal fade" id="modal-tambah" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="block-title">Tambah Admin</h3>
                <div class="block-options">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-tambah" onsubmit="return false">
                    <div class="block-content">
                        <input name="<?php echo $this->security->get_csrf_token_name(); ?>"
                            value="<?php echo $this->security->get_csrf_hash(); ?>" type="hidden" />
                        <div class="form-group">
                            <label class="text-bold">Nama</label>
                            <input type="text" name="nama" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="text-bold">Username</label>
                            <input type="text" name="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="text-bold">Password</label>
                            <input type="password" name="pass" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-md btn-success pull-right" id="tombol-submit">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-dark pull-right">
                            <i class="fa fa-close"></i> Batal
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ubah" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="block-title">Tambah Admin</h3>
                <div class="block-options">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-ubah" onsubmit="return false">
                    <div class="block-content">
                        <input name="<?php echo $this->security->get_csrf_token_name(); ?>"
                            value="<?php echo $this->security->get_csrf_hash(); ?>" type="hidden" />
                        <input type="hidden" name="id_admin" />

                        <div class="form-group">
                            <label class="control-label">Nama</label>
                            <input type="text" name="nama" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label class="text-bold">Username</label>
                            <input type="text" name="username" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input class="form-control" type="password" name="password">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Repassword</label>
                            <input class="form-control" type="password" name="repassword">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-md btn-success pull-right" id="tombol-submit">
                                <i class="fa fa-save"></i> Simpan
                            </button>
                            <button type="button" data-dismiss="modal" class="btn btn-md btn-dark pull-right">
                                <i class="fa fa-close"></i> Batal
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>