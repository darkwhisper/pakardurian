<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Petunjuk Menu Aplikasi</h4>
                <h6 class="card-subtitle">Penjelasan fitur - fitur atau menu di aplikasi beserta fungsinya</h6>
                <table data-toggle="table" data-height="300" data-mobile-responsive="true" class="table-striped">
                    <thead>
                        <tr>
                            <th>Menu</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($m_data as $v_petunjuk) {
                        ?>
                        <tr id="tr-id-1" class="tr-class-1">
                            <td><?php echo $v_petunjuk['nama_petunjuk'] ?></td>
                            <td><?php echo $v_petunjuk['ket_petunjuk'] ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>