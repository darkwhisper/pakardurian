<script>
    $(document).ready(function() {

        // set datatables menggunakan serverside
        dtpetunjuk = $("#tpetunjuk").DataTable({
            "ajax": {
                "url": "<?= base_url('petunjuk/tampil_petunjuk'); ?>",
                "type": "POST"
            },
            "serverSide": true,
            "bFilter": false,
            "lengthMenu": [[5, 25, 50, -1], [5, 10, 25, "All"]],
            "paging": true,
            "columns": [{
                    "data": "id_petunjuk"
                },
                {
                    "data": "nama_petunjuk"
                },
                {
                    "data": "ket_petunjuk"
                },
                {
                    "data": "aksi",
                    "orderable": false
                }
            ]
        });

        // ajax untuk modal tambah data 
        $('#form-tambah').submit(function() {
            var modal = '#modal-tambah';
            var form = '#form-tambah';

            $.ajax({
                url: "<?php echo base_url('petunjuk/tambah_petunjuk') ?>",
                type: "POST",
                data: $(this).serialize(),
                timeout: 5000,
                dataType: "JSON",
                success: function(data) {
                    if (data.status) {
                        notif.success(data.pesan, "Berhasil");
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        dtpetunjuk.ajax.reload(null, false);
                    } else {
                        notif.error(data.pesan, 'Gagal');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertajax.error(textStatus, jqXHR.status);
                }
            });
            $(form + ' #tombol-simpan').html('<i class="fa fa-save"></i> Simpan');
            $(form + ' #tombol-simpan').attr('disabled', false);
        });

        // ajax untuk hapus data
        $('#tpetunjuk').on('click', '.hapus-data', function() {
            /* var modal dan form */
            var data_id = $(this).attr('data-id');

            $.confirm({
                title: 'Hapus data?',
                content: 'Apakah Anda yakin akan menghapus data ini?',
                type: 'red',
                buttons: {
                    ya: {
                        btnClass: 'btn-red',
                        action: function() {
                            $.ajax({
                                type: 'post',
                                url: '<?php echo site_url('petunjuk/hapus_petunjuk '); ?>',
                                data: 'data_id=' + data_id,
                                dataType: 'JSON',
                                timeout: 5000,
                                success: function(data) {
                                    if (data.status) {
                                        notif.success(data.pesan, "Berhasil");
                                        dtpetunjuk.ajax.reload(null, false);
                                    } else {
                                        notif.error(data.pesan, "Gagal");
                                        dtpetunjuk.ajax.reload(null, false);
                                    }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    dtpetunjuk.ajax.reload(null, false);
                                    alertajax.error(textStatus, jqXHR.status);
                                }
                            });
                        }
                    },
                    batal: function() {}
                }
            });
        });

        // ajax untuk ubah data
        $('#tpetunjuk').on('click', '.ubah-data', function() {
            /* var modal dan form */
            var modal = '#modal-ubah';
            var form = '#form-ubah';

            var data_id = $(this).attr('data-id');

            $.ajax({
                url: "<?= base_url('petunjuk/get_ubahpetunjuk') ?>",
                type: "POST",
                data: "data_id=" + data_id,
                timeout: 5000,
                dataType: "JSON",
                success: function(data) {
                    if (data.status) {
                        $(form + ' [name="id_petunjuk"]').val(data.data.id_petunjuk);
                        $(form + ' [name="nama_petunjuk"]').val(data.data.nama_petunjuk);
                        $(form + ' [name="ket_petunjuk"]').val(data.data.ket_petunjuk);
               

                        $(modal).modal('show');
                    } else {
                        notif.alert('Terjadi kesalahan saat menghubungkan ke server.<br/>' +
                            data.pesan);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertajax.error(textStatus, jqXHR.status);
                }
            });
        });

        $('#form-ubah').submit(function() {
            /* var modal dan form */
            var modal = '#modal-ubah';
            var form = '#form-ubah';

            $.ajax({
                url: "<?php echo base_url('petunjuk/ubah_petunjuk') ?>",
                type: "POST",
                data: $(this).serialize(),
                timeout: 5000,
                dataType: "JSON",
                success: function(data) {
                    if (data.status) {
                        notif.success(data.pesan, "Berhasil");
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        dtpetunjuk.ajax.reload(null, false);
                    } else {
                        notif.error(data.pesan, 'Gagal');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertajax.error(textStatus, jqXHR.status);
                }
            });
            $(form + ' #tombol-submit').attr('disabled', false);
        })

    })
</script>