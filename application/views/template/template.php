<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex, nofollow" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/images/durian.png') ?>">
    <!-- Favicon icon -->
    <!--  <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/durian.png"> -->
    <title><?php echo $this->template->title->default("Default title"); ?></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/operator/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/operator/plugins/datatables.net-bs4/css/dataTables.bootstrap4.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/operator/plugins/datatables.net-bs4/css/buttons.bootstrap4.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/global/toastr/toastr.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/global/jquery-confirm-master/dist/jquery-confirm.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/bootstrap-table/dist/bootstrap-table.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/select2/select2.min.css') ?>">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/operator/css/style.css') ?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/operator/css/colors/yellow.css') ?>" id="theme" rel="stylesheet">
    <style>

    </style>
</head>

<body class="fix-header card-no-border fix-sidebar">
    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= site_url('/') ?>">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?= base_url('assets/image/durian9.png') ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?= base_url('assets/image/durian8.png') ?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>
                            <!-- dark Logo text -->
                            <img src="<?= base_url('assets/image/logo-text.png') ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo text -->
                            <img src="<?= base_url('assets/image/logo-light-text.png') ?>" class="light-logo" alt="homepage" /></span> </a>
                    <br>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                    </ul>

                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->

                    <?php
                    if (!empty($this->session->userdata('id_admin'))) {
                    ?>
                        <ul class="navbar-nav my-lg-0">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="<?= base_url('assets/image/users/do7.png') ?>" alt="user" class="profile-pic" /></a>
                                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                    <ul class="dropdown-user">
                                        <li>
                                            <div class="dw-user-box">
                                                <div class="u-img"><img src="<?= base_url('assets/image/users/do7.png') ?>" alt="user"></div>
                                                <div class="u-text">

                                                </div>
                                            </div>
                                        </li>
                                        <li role="separator" class="divider"></li>

                                        <li><a href=""><i class=""></i>
                                                Hai! <?php echo $this->session->userdata('user_name') ?></a>
                                        <li><a href="<?= site_url('diagnosis/logout') ?>"><i class="fa fa-power-off"></i>
                                                Keluar</a>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    <?php
                    }

                    ?>

                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>
                        <li>
                            <a class="has-arrow" href="<?= site_url('/') ?>" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Beranda </span></a>

                        </li>

                        <?php
                        if (empty($this->session->userdata('id_admin'))) {

                        ?>

                            <li>
                                <a class="has-arrow " href="<?= site_url('petunjuk/user_petunjuk') ?>" aria-expanded="false"><i class="mdi mdi-calendar-blank"></i><span class="hide-menu">Petunjuk</span></a>
                            </li>
                            <li>
                                <a class="has-arrow " href="<?= site_url('penyakit/user_penyakit') ?>" aria-expanded="false"><i class="mdi mdi-bug"></i><span class="hide-menu">Daftar Penyakit</span></a>
                            </li>


                        <?php } else { ?>

                            <li>
                                <a class="has-arrow " href="<?= site_url('admin') ?>" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Data Admin</span></a>
                            </li>
                            <li>
                                <a class="has-arrow " href="<?= site_url('penyakit') ?>" aria-expanded="false"><i class="mdi mdi-bug"></i><span class="hide-menu">Data Penyakit</span></a>
                            </li>
                            <li>
                                <a class="has-arrow " href="<?= site_url('gejala') ?>" aria-expanded="false"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu">Data Gejala</span></a>
                            </li>
                            <li>
                                <a class="has-arrow " href="<?= site_url('aturan') ?>" aria-expanded="false"><i class="mdi mdi-brightness-5"></i><span class="hide-menu">Data Aturan</span></a>
                            </li>
                            <li>
                                <a class="has-arrow " href="<?= site_url('petunjuk') ?>" aria-expanded="false"><i class="mdi mdi-calendar-blank"></i><span class="hide-menu">Data Petunjuk</span></a>
                            </li>
                        <?php } ?>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <div class="page-wrapper">
            <div class="container-fluid">

                <?php
                echo $this->template->content;
                ?>

                <footer class="footer">
                    © 2020 Sipakar Durian </a>
                </footer>
            </div>
        </div>
    </div>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= base_url('assets/operator/plugins/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/plugins/bootstrap/js/popper.min.js') ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= base_url('assets/operator/js/jquery.slimscroll.js') ?>"></script>
    <!--Wave Effects -->
    <script src="<?= base_url('assets/operator/js/waves.js') ?>"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url('assets/operator/js/sidebarmenu.js') ?>"></script>
    <!--stickey kit -->
    <script src="<?= base_url('assets/operator/plugins/sticky-kit-master/dist/sticky-kit.min.js') ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?= base_url('assets/operator/js/custom.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/js/circlelabs-custom.js') ?>"></script>
    <script src="<?= base_url('assets/operator/plugins/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/plugins/datatables.net/js/dataTables.buttons.min.js') ?>"></script>
    <script src="<?= base_url('assets/operator/js/jspdf.min.js') ?>"></script>
    <script src="<?= base_url('assets/global/toastr/toastr.min.js') ?>"></script>
    <script src="<?= base_url('assets/global/jquery-confirm-master/dist/jquery-confirm.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/bootstrap-table/dist/bootstrap-table.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/bootstrap-table/dist/bootstrap-table-locale-all.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/select2/select2.min.js') ?>"></script>
    <script src="<?= base_url('assets/global/dist/extensions/export/bootstrap-table-export.min.js') ?>"></script>

    </script>
    <?php
    echo $this->template->js;
    ?>

</body>

</html>